<?php


namespace App\Http\Controllers;


use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(){
        return response()->json(Auth::user(), 200);
    }

    public function update(Request $request)
    {
        // todo
    }

    public function lostCup(){
        Auth::invalidate();
        return response()->json(['message' => 'SUCCESS'], 200);
    }

    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            if ($user == Auth::user()) {
                $user->delete();
                return response('', 204);
            } else {
                return response('Unauthorized.', 401);
            }
        } catch (Exception $e) {
            return response()->json(['message' => 'User not found!'], 404);
        }
    }
}
