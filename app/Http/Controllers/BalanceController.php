<?php


namespace App\Http\Controllers;


use App\Admin;
use App\Boisson;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BalanceController extends Controller
{
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'boissons' => 'required|json',
            'tokenUser' => 'required|string'
        ]);

        $admin = Admin::findOrFail(Auth::user()->id);
        $user = User::findOrFail($id);

//        Auth::shouldUse('auth');
//        $token = Auth::tokenById($id);
//
//        if($token != $request->input('tokenUser')){
//            return response()->json(['message' => 'FAILED'], 400);
//        }


        if (abs($request->input('amount')) <= abs($user->balance)) {
            $boissons = json_decode($request->input('boissons'), true);
            foreach ($boissons as $b) {
                $boisson = Boisson::findOrFail($b['id']);

                for ($i = 0; $i < $b['count']; $i++) {
                    PaiementController::store($user->id, -$boisson->price, $admin->bar->id, $boisson->id);
                }
            }

            $user->balance -= $request->input('amount');
            $user->save();
            return response()->json(['message' => 'SUCCESS'], 200);
        } else {
            return response()->json(['message' => 'FAILED'], 400);
        }
    }
}

