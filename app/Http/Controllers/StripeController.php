<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Exception\SignatureVerificationException;
use Stripe\PaymentIntent;
use Stripe\Webhook;
use UnexpectedValueException;

class StripeController extends Controller
{

    public function charge(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|integer'
        ]);

        $intent = PaymentIntent::create([
            'amount' => $request->input('amount'),
            'currency' => 'eur',
            'customer' => Auth::user()->customerId
        ]);

        return response()->json(['publishable_key' => env('STRIPE_KEY'), 'client_secret' => $intent->client_secret], 200);
    }

    public function getKey()
    {
        return response()->json(['publicKey' => env('STRIPE_KEY')]);
    }

    public function webhook(Request $request)
    {
        $payload = $request->getContent();
        $event = null;

        try {
            // Make sure the event is coming from Stripe by checking the signature header
            $event = Webhook::constructEvent($payload, $_SERVER['HTTP_STRIPE_SIGNATURE'], env('STRIPE_WH_SECRET'));
        } catch (UnexpectedValueException $e) {
            return response()->json(['message' => 'Badly formatted JSON'], 400);
        } catch (SignatureVerificationException $e) {
            return response()->isForbidden();
        }

        if ($event->type == 'payment_intent.succeeded') {
            $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
            $this->updateBalance($paymentIntent->customer, $paymentIntent->amount);
        } else if ($event->type == 'payment_intent.payment_failed') {
            //todo
        }
    }

    public function updateBalance($customerId, $amount)
    {
        $user = User::where('customerId', '=', $customerId)->firstOrFail();
        PaiementController::store($user->id, $amount/100);
        $user->balance += $amount / 100;
        $user->save();
    }
}

