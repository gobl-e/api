<?php

namespace App\Console;

use Everyman\Neo4j\Cypher\Query;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $client = DB::connection('neo4j')->getClient();
            $queryString = "MATCH (n) WHERE NOT labels(n) delete (n)";
            $query = new Query($client, $queryString);
            $result = $query->getResultSet();
        })->daily();
    }
}
