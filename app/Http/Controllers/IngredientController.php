<?php


namespace App\Http\Controllers;


use App\Ingredient;
use Exception;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    public function show($id)
    {
        try {
            $ingredient = Ingredient::findOrFail($id);
            return response()->json(['message' => 'SUCCESS', 'ingredient' => $ingredient], 200);
        } catch (Exception $e) {
            return response()->json(['message' => 'Ingredient not found!'], 404);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $ingredient = new Ingredient;
        $ingredient->name = $request->input('name');
        $ingredient->save();

        return response()->json(['ingredient' => $ingredient, 'message' => 'CREATED'], 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        try {
            $ingredient = Ingredient::findOrFail($id);
            $ingredient->name = $request->input('name');
            $ingredient->save();
            return response()->json(['message' => 'SUCCESS', 'ingredient' => $ingredient], 200);
        } catch (Exception $e) {
            return response()->json(['message' => 'Ingredient not found!'], 404);
        }
    }

    public function destroy($id)
    {
        try {
            $ingredient = Ingredient::findOrFail($id);
            $ingredient->delete();
            return response('', 204);
        } catch (Exception $e) {
            return response()->json(['message' => 'Ingredient not found!'], 404);
        }
    }
}
