<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Bar extends NeoEloquent
{
    protected $label = 'Bar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function boissons(){
        return $this->hasMany('App\Boisson');
    }
}
