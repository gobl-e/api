<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Boisson extends NeoEloquent
{
    protected $label = 'Boisson';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'bgColor', 'textColor'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function ingredients()
    {
        return $this->hasMany('App\Ingredient');
    }

    public function bar()
    {
        return $this->belongsTo('App\Bar');
    }
}
