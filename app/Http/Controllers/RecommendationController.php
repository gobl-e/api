<?php


namespace App\Http\Controllers;


use App\User;
use Everyman\Neo4j\Cypher\Query;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecommendationController extends Controller
{
    /**
     * Instantiate a new PaiementController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $client = DB::connection('neo4j')->getClient();

        $user = User::findOrFail(Auth::user()->id);

        $queryString = "MATCH (p:Paiement)<-[rel:PAIEMENTS]-(u:User) WHERE ID(u) = " . $user->id . " RETURN p.boissonId, count(rel) as boissons order by boissons desc LIMIT 3";
        $query = new Query($client, $queryString);
        $results = $query->getResultSet();

        $boissonsIds = [];
        foreach ($results as $result) {
            $boissonsIds[] = $result['p.boissonId'];
        }
        $stringIds = implode(",", array_filter($boissonsIds));

        $queryString = 'MATCH (i:Ingredient)<-[rel:INGREDIENTS]-(b:Boisson) WHERE ID(b) IN [' . $stringIds . '] WITH collect(i.name) as listIngredients MATCH (b:Boisson) WHERE NOT ID(b) IN [' . $stringIds . '] AND any(i in listIngredients WHERE exists((b)-[:INGREDIENTS]->(:Ingredient {name: i}))) RETURN b.name AS boisson, [(b)-[:INGREDIENTS]->(i) | i.name] AS ingredients ORDER BY size(ingredients) LIMIT 3';
        $query = new Query($client, $queryString);
        $results = $query->getResultSet();

        $boissons = [];
        foreach ($results as $result) {
            $boissons[] = $result['boisson'];
        }

        return response()->json(['message' => 'SUCCESS', 'boissons' => $boissons], 200);
    }
}
