<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function profile()
    {
        return response()->json(Auth::user(), 200);
    }
}
