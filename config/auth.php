<?php

use App\User;
use App\Admin;

return [
    'defaults' => [
        'guard' => 'users',
        'passwords' => 'users',
    ],

    'guards' => [
        'admins' => [
            'driver' => 'jwt',
            'provider' => 'admins',
        ],
        'users' => [
            'driver' => 'jwt',
            'provider' => 'users',
        ],
    ],

    'providers' => [
        'admins' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ],

        'users' => [
            'driver' => 'eloquent',
            'model' => User::class
        ]
    ]
];
