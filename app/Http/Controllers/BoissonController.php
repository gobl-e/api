<?php


namespace App\Http\Controllers;


use App\Boisson;
use App\Ingredient;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoissonController extends Controller
{
    public function index()
    {
        $boissons = Auth::user()->bar->boissons()->get();
        return response()->json(['message' => 'SUCCESS', 'boissons' => $boissons], 200);
    }

    public function show($id)
    {
        $boisson = Auth::user()->bar->boissons()->where('id', $id)->first();

        if (empty($boisson)) {
            return response()->json(['message' => 'Boisson not found!'], 404);
        }
        return response()->json(['message' => 'SUCCESS', 'boisson' => $boisson], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $boisson = new Boisson;
        $boisson->name = $request->input('name');
        $boisson->price = 0.0;
        $boisson->bgColor = "1C212B";
        $boisson->textColor = "FFFFFF";
        $boisson->save();

        Auth::user()->bar->boissons()->save($boisson);

        return response()->json(['boisson' => $boisson, 'message' => 'CREATED'], 201);
    }

    public function addIngredient($bid, $iid)
    {
        $boisson = null;
        $ingredient = null;

        $boisson = Auth::user()->bar->boissons()->where('id', $bid)->first();
        if (empty($boisson)) {
            return response()->json(['message' => 'Boisson not found!'], 404);
        }

        try {
            $ingredient = Ingredient::findOrFail($iid);
        } catch (Exception $e) {
            return response()->json(['message' => 'Ingredient not found!'], 404);
        }

        $boisson->ingredients()->save($ingredient);
        return response()->json(['message' => 'ADDED'], 200);
    }

    public function removeIngredient($bid, $iid)
    {
        $boisson = null;
        $ingredient = null;

        $boisson = Auth::user()->bar->boissons()->where('id', $bid)->first();
        if (empty($boisson)) {
            return response()->json(['message' => 'Boisson not found!'], 404);
        }

        try {
            $ingredient = Ingredient::findOrFail($iid);
        } catch (Exception $e) {
            return response()->json(['message' => 'Ingredient not found!'], 404);
        }

        $boisson->ingredients()->detach($ingredient);
        return response()->json(['message' => 'REMOVED'], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'nullable|string',
            'price' => 'nullable|numeric',
            'bgColor' => 'nullable|string|max:6',
            'textColor' => 'nullable|string|max:6'
        ]);

        $boisson = Auth::user()->bar->boissons()->where('id', $id)->first();
        if (empty($boisson)) {
            return response()->json(['message' => 'Boisson not found!'], 404);
        }

        if ($request->has('name')) {
            $boisson->name = $request->input('name');
        }

        if ($request->has('price')) {
            $boisson->price = $request->input('price');
        }

        if ($request->has('bgColor')) {
            $boisson->bgColor = $request->input('bgColor');
        }

        if ($request->has('textColor')) {
            $boisson->textColor = $request->input('textColor');
        }
        $boisson->save();
        return response()->json(['message' => 'SUCCESS', 'boisson' => $boisson], 200);
    }

    public function destroy($id)
    {
        $boisson = Auth::user()->bar->boissons()->where('id', $id)->first();
        if (empty($boisson)) {
            return response()->json(['message' => 'Boisson not found!'], 404);
        }
        $boisson->delete();
        return response('', 204);
    }
}
