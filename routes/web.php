<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/user/profile', 'UserController@profile');
$router->put('/user/{id}', 'UserController@update');
$router->delete('/user/{id}', 'UserController@destroy');


$router->post('/register', 'AuthController@register');
$router->post('/login', 'AuthController@login');
$router->get('/logout', 'AuthController@logout');

$router->post('/charge', ['middleware' => 'auth', 'uses' => 'StripeController@charge']);
$router->post('/payment-events', 'StripeController@webhook');

$router->put('/user/{id}/balance', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BalanceController@update']);
$router->get('/lost-cup', 'UserController@lostCup');

$router->get('/admin/profile', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'AdminController@profile']);

$router->get('/boisson', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@index']);
$router->get('/boisson/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@show']);
$router->post('/boisson', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@store']);
$router->put('/boisson/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@update']);
$router->delete('/boisson/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@destroy']);

$router->get('/ingredient', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'IngredientController@index']);
$router->get('/ingredient/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'IngredientController@show']);
$router->post('/ingredient', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'IngredientController@store']);
$router->put('/ingredient/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'IngredientController@update']);
$router->delete('/ingredient/{id}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'IngredientController@destroy']);

$router->post('/boisson/{bid}/ingredient/{iid}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@addIngredient']);
$router->delete('/boisson/{bid}/ingredient/{iid}', ['middleware' => ['assign.guard:admins', 'jwt.auth'], 'uses' => 'BoissonController@removeIngredient']);

$router->get('/user/paiement', 'PaiementController@index');

$router->get('/recommendation', 'RecommendationController@index');
