<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;
use App\Admin;

class Ingredient extends NeoEloquent
{
    protected $label = 'Ingredient';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function boissons()
    {
        return $this->belongsToMany('App\Boisson');
    }
}
