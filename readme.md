# Gobl-e API

Ce dépôt contient les sources de l'API de la solution Gobl-e. 
Le framework PHP Lumen a été utilisé.

## Installation
### Pré-requis (versions minimales)

 * Windows 10 / Linux (Équivalent Ubuntu 18.04 ou supérieur) / MacOS (Mojave ou supérieur) 
 * PHP version 7.3.1
 * Composer version 1.9
 * Neo4J version 3.5.14
 
### Installation

* Installer dans l'ordre sur la machine : PHP, Neo4j, Composer.
* Cloner le projet git dans un répertoire de votre choix.
* Créer dans ce dossier du projet un fichier .env :
```
APP_NAME=Lumen
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=neo4j
DB_HOST=127.0.0.1
DB_PORT=7474
DB_USERNAME=
DB_PASSWORD=

CACHE_DRIVER=file
QUEUE_CONNECTION=sync

JWT_SECRET=

STRIPE_KEY=your-stripe-key
STRIPE_SECRET=your-stripe-secret
STRIPE_WH_SECRET=your-stripe-webhook-secret
```
* Compléter ces lignes :
```
DB_USERNAME=_VotreNom_
DB_PASSWORD=_VotrePwd_
```
* Exécuter ces commandes, à la racine du projet :  
```
composer install
php artisan neo4j:migrate
php artisan jwt:secret
```

* Générer une clé de 32 caractères et la rentrer dans le champ ([générateur](http://www.unit-conversion.info/texttools/random-string-generator/)) :
```
APP_KEY=_VotreCléGénérée_
```

Finalement :
* Lancer la commande `php -S localhost:8000 -t public` et ouvrir l'url http://127.0.0.1:8000


### Commandes disponibles

Exécuter toutes les migrations : `php artisan neo4j:migrate`
Seeder la base : `php artisan db:seed`



## Auteurs

**Johann Carfantan** ; **Emma Pardo** ; **Jochen Rousse** ; **Lionel Sermanson**



