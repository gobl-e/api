<?php

use Illuminate\Database\Seeder;

use App\Bar;
use App\Admin;
use App\Ingredient;
use App\Boisson;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bar = Bar::create([
            'name' => 'WarpZone Lannion'
        ]);

        $admin = Admin::create([
            'name' => 'Jean',
            'surname' => 'Admin',
            'birthDate' => '10/03/1997',
            'email' => 'jean.admin@gmail.com',
            'password' => app('hash')->make('jeanadmin')
        ]);

        $admin->bar()->save($bar);

        $boisson1 = Boisson::create([
            'name' => 'mojito',
            'price' => 5.0,
            'bgColor' => '1C212B',
            'textColor' => 'FFFFFF'
        ]);

        $boisson2 = Boisson::create([
            'name' => 'rhum coca',
            'price' => 3.0,
            'bgColor' => '1C212B',
            'textColor' => 'FFFFFF'
        ]);

        $ingredient1 = Ingredient::create([
            'name' => 'rhum'
        ]);

        $ingredient2 = Ingredient::create([
            'name' => 'menthe'
        ]);

        $ingredient3 = Ingredient::create([
            'name' => 'sucre'
        ]);

        $ingredient4 = Ingredient::create([
            'name' => 'coca'
        ]);

        $boisson1->ingredients()->save($ingredient1);
        $boisson1->ingredients()->save($ingredient2);
        $boisson1->ingredients()->save($ingredient3);

        $boisson2->ingredients()->save($ingredient1);
        $boisson2->ingredients()->save($ingredient4);

        $bar->boissons()->save($boisson1);
        $bar->boissons()->save($boisson2);
    }
}
