<?php


namespace App\Http\Controllers;


use App\Bar;
use App\Boisson;
use App\Paiement;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class PaiementController extends Controller
{
    /**
     * Instantiate a new PaiementController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $paiements = Auth::user()->paiements()->get();

        foreach ($paiements as $paiement) {
            $paiement->boisson = "Crédit";
            $paiement->bar = "Carte bancaire";

            if (isset($paiement->boissonId)) {
                $boisson = Boisson::find($paiement->boissonId);
                $paiement->boisson = $boisson->name;
            }
            if (isset($paiement->barId)) {
                $bar = Bar::find($paiement->barId);
                $paiement->bar = $bar->name;
            }
        }

        return response()->json(['message' => 'SUCCESS', 'paiements' => $paiements], 200);
    }

    public static function store($userId, $amount, $barId = null, $boissonId = null)
    {
        $paiement = new Paiement;
        $paiement->amount = $amount;
        $paiement->barId = $barId;
        $paiement->boissonId = $boissonId;
        $paiement->save();

        try {
            $user = User::findOrFail($userId);
        } catch (Exception $e) {
            return ($e);
        }

        $user->paiements()->save($paiement);
    }
}
